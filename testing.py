import csv
import h5py
import numpy as np
import pandas as pd
import itertools
import re
import json
import os
import util

# def transformCSVToDataSet(csvName):
#     f = h5py.File("mytestfile.hdf5", "w")
#     arr = np.genfromtxt(csvName, delimiter=',', dtype=None)
#     print(arr)
#     f.create_dataset("init", data=arr)



# def readhdf5(hdf5Name):
#     f = h5py.File(hdf5Name, "r")
#     d = f["init"]
#     print(type(d[0][0]))

# def test5(hdf5Name):
#     a=np.array([1,2,3,4,5,6,7,8,9])
#     b=np.array(["a","b","c","d","e","f","g","h","i"])
#     c=np.array([9,8,7,6,5,4,3,2,1])
#     datatype = np.dtype({'names':['a1','a2', 'a3'], 'formats':[np.uint8,'S1',np.uint8]})
#     k = np.fromiter(zip(a,b,c),dtype=datatype )
#     p = np.rec.fromarrays([a,b,c], formats=[np.uint8,'S1',np.uint8], names=['num','char','len'])
#     f = h5py.File(hdf5Name, "w")
#     f.create_dataset("init", data=k)


# userPath = "hello"

# p = re.findall(r'[^A-Za-z0-9_\-\\]',userPath)
# print(p)


# # json testing
# f = open('tj.json')
  
# # returns JSON object as 
# # a dictionary
# data = json.load(f)
# print(data["nextLevel"][0].keys())


# for root, dirs, files in os.walk(r"C:\Users\Mary Hayhoe\Desktop\Curl Project_11012022_broken\Assets\Data\ABB", topdown=True):
#    for name in files:
#       print(os.path.join(root, name))
#    for name in dirs:
#       print(os.path.join(root, name))
    
# print(next(os.walk(r"C:\Users\Mary Hayhoe\Desktop\Curl Project_11012022_broken\Assets\Data\ABB"))[1])
# print(next(os.walk(r"C:\Users\Mary Hayhoe\Desktop\Curl Project_11012022_broken\Assets\Data\ABB"))[1])
# for x in os.listdir(r"C:\Users\Mary Hayhoe\Desktop\Curl Project_11012022_broken\Assets\Data\ABB\09072022_202539"):
#     if x.endswith(".csv"):
#         print(x)
import csv
# with open(r"C:\Users\Mary Hayhoe\Desktop\Curl Project_11012022_broken\Assets\Data\ABB/04072022_212948/config.csv", "r") as csvfile:
#     reader_variable = csv.reader(csvfile, delimiter=",")
#     for row in reader_variable:
#         print(row)
# np.genfromtxt(r"C:\Users\Mary Hayhoe\Desktop\Curl Project_11012022_broken\Assets\Data\ABB/04072022_212948/config.csv",delimiter=',', dtype=None)

util.loadGroupFolder(r"J:\VrLab\Data", r"J:\VrLab")

# print(util.loadConfig(r"C:\Users\Mary Hayhoe\Desktop\Curl Project_11012022_broken\Assets\Data\ABB/04072022_212948/config.csv"))