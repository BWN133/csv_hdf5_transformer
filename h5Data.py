
class h5DataNode():
    def __init__(self, isRoot=False, path:str="", parent:h5DataNode=None,child:List[h5DataNode]=[], files:List[str]=[]):
        self.childs = child
        self.parent = parent
        self.files = files
        self.name = path
        
    
    def setChilds(self, newChilds:List[h5DataNode]):
        self.childs = newChilds
    
    def addChild(self, child:h5DataNode):
        self.childs.append(h5DataNode)
    
    def setParent(self, newParent:h5DataNode):
        self.parent = newParent
    
    def setFiles(self, newFiles:List[str]):
        self.files = newFiles
    
    def addFiles(self, file:str):
        self.files.append(file)

    def setPath(self, path:str):
        self.path = path
    
    def __str__(self):
        pass
        



class h5Tree():
    # O(1) retrieve node name and node to set parent and child
    def __init__(self, root:h5DataNode):
        self.root = root
    
    # # def getRoot()
    # def createTree(self):
        
        