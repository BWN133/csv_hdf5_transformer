import argparse
import util
import re
import os

parser = argparse.ArgumentParser(description='CSV To HDF5 Library')
parser.add_argument('-s', dest="configPath", type=str, help="providing a script name if you want to use this in script")
parser.add_argument('-d',dest='dataPathName', metavar='P', type=str, help='path name of dataset')
parser.add_argument('-f', dset='folderName', help='Helper Name')
parser.add_argument('-c', dest='csvName', help='target CSV Name')
parser.add_argument('--h5', dest='hdf5Name', required=True, default="default.hdf5" , help='hdf5 file name')
args = parser.parse_args()
# Check for legal input
# if args.configPath is None and len(re.findall(r'[^A-Za-z0-9_\-\\]', args.dataPathName)) != 0:
#     print("Please Entter legal data Path Name")
#     exit()

# if args.configPath is None and not os.path.exists(args.csvName):
#     print(args.csvName)
#     print("Please Enter Valid CSV Path")
#     exit()


if args.configPath is not None:
    util.transformDataSetWithConfig(args.configPath, args.hdf5Name)
else:
    f = util.initializeHDF5(args.hdf5Name)
    util.transformCSVToDataSet(args.csvName,f, args.dataPathName)
    f.close()
    

