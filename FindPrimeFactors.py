# File: FindPrimeFactors.py
# Student: 
# UT EID:
# Course Name: CS303E
# 
# Date:
# Description of Program: 
import math
def isPrime ( num ):
    """ Test whether num is prime . """
    # Deal with evens and numbers < 2.
    if ( num < 2 or num % 2 == 0 ):
        return ( num == 2 )
    # See if there are any odd divisors
    # up to the square root of num.
    divisor = 3
    while ( divisor <= math.sqrt ( num )):
        if ( num % divisor == 0 ) :
            return False
        else :
            divisor += 2
    return True

def findNextPrime ( num ):
    """ Find the first prime > num. """
    if ( num < 2 ) :
        return 2
    # If (num >= 2 and num is even ), the
    # next prime after num is at least
    # (num - 1) + 2 , which is odd.
    if ( num % 2 == 0) :
        num -= 1
    guess = num + 2
    while ( not isPrime ( guess )):
        guess += 2
    return guess
print("Find Prime Factors:")
while True:
    ans = []
    d = 2
    inp = int(input("Enter a positive integer (or 0 to stop):"))
    org = inp
    if inp == 0: 
        break
    if inp < 0:
        print("Negative integer entered. Try again.")
        print()
        continue
    if isPrime(inp):
        ans.append(inp)
    else:
        while inp > 1:
            if inp % d == 0:
                ans.append(d)
                inp /= d
            else:
                d = findNextPrime(d)
    if len(ans) == 0:
        print("1 has no prime factorization")
    else:
        print("The prime factorization of", org, "is:", ans)
    print()
print("Goodbye!")
                