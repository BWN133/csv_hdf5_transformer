import csv
import h5py
import numpy as np
import pandas as pd
import itertools
import json
import os



def transformCSVToDataSet(csvName, hdf5F, path:str, dtype=None, attributes:dict=None):
    """
    Transform one csv dataset into hdf5 dataset with given path
    """
    data = getAndCheckCSVData(csvName)
    if data.ndim == 1:
        print("detected empty data set, jump")
        return
    header = data[0]
    firstRow = data[1]
    data = data[1:]
    data = data.transpose()
    dataType = createDtype(firstRow, header)
    
    annotatedData = np.rec.fromarrays(data, dtype=dataType)
    if path in hdf5F:
        del hdf5F[path]
    dset = hdf5F.create_dataset(path, data=annotatedData)
    addATT(dset, attributes)

def testingDataType(firstRow):
    result = []
    for d in firstRow:
        if str(d)[2:-1].replace(".","").replace("-","").isnumeric():
            result.append(np.float32)
        else:
            result.append((np.unicode))
    return result

def createDtype(firstRow, header):
    # print("In createDtype firstRow", firstRow)
    # print("in CreateDtype header", header)
    result = []
    for i in range(len(header)):
        value = firstRow[i]
        if str(value)[2:-1].replace(".","").replace("-","").isnumeric():
            result.append((str(header[i]), np.float32))
        else:
            result.append((str(header[i]), 'S10'))
    return np.dtype(result)
        
    

def addATT(dset, attributes):
    if attributes:
        for key in attributes.keys():
            dset.attrs[key] = attributes[key]

def getAndCheckCSVData(csvName):
    """
    extract csv data while checking fiel legality
    """
    print(csvName + " in get and check")
    data = np.genfromtxt(csvName, delimiter=',', dtype=None)
    # try:
    #     data = np.genfromtxt(csvName, delimiter=',', dtype=None)
    # except:
    #     print("only accepting CSV transforming now")
    #     exit()
    return data


def transformDataSetWithConfig(jsonPath, hdf5FileName):
    f = open(jsonPath)
    jsonConfig = json.load(f)
    hdf5F =  initializeHDF5(hdf5FileName)
    loadGroupHelper(hdf5F, jsonConfig, "")
    f.close()


def initializeHDF5(hdf5FileName):
    try:
        hdf5F =  h5py.File(hdf5FileName, 'a')
    except:
        print("h5py file initialization failed, please put in valid input for hdf5 name. Example: example.hdf5")
        exit()
    return hdf5F


# Recursion without any dp is really bad design
def loadGroupHelper(hdf5F, jsonConfig, groupPath):
    attributes = {}
    for key in jsonConfig.keys():
        curValue = jsonConfig[key]
        curPath = getNewGroupPath(groupPath, key)
        if type(curValue) is list and type(curValue[0]) is dict:
            for tempD in curValue:
                loadGroupHelper(hdf5F, tempD, curPath)
        elif type(curValue) is str:
            if os.path.isfile(curValue):
                transformCSVToDataSet(curValue, hdf5F, curPath, attributes=attributes)
            else:
                attributes[key] = curValue
                

def getNewGroupPath(groupPath, key):
    if not len(groupPath):
        return str(key)
    return groupPath + '/' + str(key)

def loadGroupFolder(rootFolderName, exportPath=None):
    for file in os.listdir(rootFolderName):
        d = os.path.join(rootFolderName, file)
        e = os.path.join(exportPath, file) if exportPath else d
        if os.path.isdir(d):
            loadFolder(e + '.h5', d)

def loadFolder(hdf5FName, rootFolderName):
    hdf5F =  initializeHDF5(hdf5FName)
    loadFolderHelper(hdf5F, rootFolderName, "", loadConfigB=True)
    hdf5F.close()
    

def loadConfig(config, attributes:dict):
    with open(config, "r") as csvfile:
        reader_variable = csv.reader(csvfile, delimiter=",")
        for row in reader_variable:
            if len(row) == 2:
                attributes[row[0]] = row[1]
            else:
                attributes[row[0]] = [i for i in row[1:]]
    return attributes

def loadFolderHelper(hdf5F, path,groupPath, loadConfigB=False):
    print(path)
    for subfolder in next(os.walk(path))[1]:
        loadFolderHelper(hdf5F, getNewGroupPath(path, subfolder), getNewGroupPath(groupPath, subfolder))
    attributes = {}
    for x in os.listdir(path):
        if x.endswith(".csv"):
            print(x)
            if x == "config.csv" and loadConfig:
                loadConfig(getNewGroupPath(path, x), attributes)
            else:
                transformCSVToDataSet(getNewGroupPath(path, x), hdf5F,getNewGroupPath(groupPath, x), attributes=attributes)


